/*
 *  Fecha:
 *  
 *  Autor:
 *  
 *  Descripción: Estado de conexión
 */
#include "WiFi.h"
#include "WebServer.h"


const char * ssid="hercab2k20";
const char * passwd="AxVa9295";

WebServer server(80);  // Crear servidor que escucha peticiones por el puerto 80

String pagina="<!DOCTYPE html>"
              "<html>"
              "<head>"
              "<title>ESP32 4</title>"
              "</head>"
              "<body>"
              "<center><h1>Formulario</h1></center>"
              "<hr/>"
              "<center>"
              "<form action='/calcula' method='post'>"
              "<fieldset>"
              "<legend>Calculadora:</legend>"
              "<label>Proporciona un n&uacute;mero:</label>"
              "<input type='number' value='0' name='numero1'><br/><br/>"
              "<label>Proporciona otro n&uacute;mero:</label>"
              "<input type='number' value='0' name='numero2'><br/><br/>"
              "<label>Selecciona la operacion:</label>"
              "<select name='operacion'>"
              "<option value='suma'>suma</option>"
              "<option value='resta'>resta</option>"
              "<option value='producto'>producto</option>"
              "<option value='divide'>divide</option>"
              "</select><br/><br/>"
              "<input type='reset' value='limpiar'>"
              "<input type='submit' value='Enviar'><br/>"
              "</fieldset>"
              "</form>"
              "</center>"
              "</body>"
              "</html>";




void localIP(){
  Serial.print("IP asignada:");
  Serial.println(WiFi.localIP());
}

void wifiInit(){
    int intentos =0;
    // Establece modo de operación Estación y desconecta el modulo WiFi si estuviera conectado
    WiFi.mode(WIFI_STA);
    WiFi.disconnect();
    // Conexión a red específica.
    WiFi.begin(ssid,passwd);
    Serial.print("Conectando .");
    while(WiFi.status() != WL_CONNECTED && intentos++!=30){
      Serial.print(".");
      delay(1000);
    }
    Serial.println();
    if(WiFi.status()== WL_CONNECTED){
        Serial.println("Conexión completada correctamente!!");
        localIP();
    }
    else{
        Serial.print("Tiempo sobrepasado para conectarse a:");
        Serial.println(ssid);
    }
    
}



void gestorRaiz(){
      server.send(200,"text/html",pagina);
}


void gestorNoLocalizado(){
  String message = "Recurso Solicitado\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMetodo: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArgumentos: ";
  message += server.args();
  message += "\n";

  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }

  server.send(404, "text/plain", message);
}

void gestorFormulario(){
  float num1=0,num2=0;
  String operacion;
  String pagina="<!DOCTYPE html>"
                "<html"
                "<head>"
                "<title>ESP32 4</title>"
                "</head>"
                "<body>"
                "<center><h1>Formulario</h1></center>"
                "<hr/>"
                "<center>"
                "<p> Resultado=";
  if(server.args()>0){
        num1=server.arg("numero1").toFloat();
        num2=server.arg("numero2").toFloat();
        operacion=server.arg("operacion");
        if(operacion=="suma")
            num1=num1+num2;
        else if(operacion=="resta")
            num1=num1-num2;
        else if(operacion=="producto")
            num1=num1*num2;
        else 
            num1=num1/num2;
  }
  pagina+=num1;
  pagina+="<br/>"
          "<a href='/'>Regresar</a>"
          "</center></p>"
          "</body>"
          "</html>";

  server.send(200,"text/html",pagina);
  
}



void webInit(){
  server.on("/",gestorRaiz);
  server.on("/calcula",gestorFormulario);
  server.onNotFound(gestorNoLocalizado);
  server.begin();
  Serial.println("Servidor web ha sido iniciado");
}

void setup()
{
    Serial.begin(115200);
    delay(100);
    wifiInit();
    if(WiFi.status()==WL_CONNECTED)
        webInit();
}



void loop()
{
   server.handleClient();
   delay(2);
}
